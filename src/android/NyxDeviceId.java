package hu.dpal.phonegap.plugins;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.provider.Settings.Secure;
import android.util.Log;
import android.os.Environment;
import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
// import android.provider.Settings;
import android.provider.MediaStore;
import android.content.ContentValues;
import android.content.ContentResolver;
import android.net.Uri;
import android.content.ContentUris;
import android.database.Cursor;
import android.provider.Settings;
import android.content.Intent;

import androidx.core.app.ActivityCompat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.UUID;
import java.io.OutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.net.URI;

public class NyxDeviceId extends CordovaPlugin {

    public static final String TAG = "NyxDeviceId";
    private static String sID = null;
    private static final String DEVICE_ID_FILE = "DEVICEIDFILE";
    public CallbackContext callbackContext;

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.MANAGE_EXTERNAL_STORAGE,
            // ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION
    };

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        this.callbackContext = callbackContext;
        try {
            if (action.equals("get")) {
                getDeviceId();
            } else if (action.equals("save")) {
                String deviceId = args.getString(0);
                saveDeviceId(deviceId);
            } else if (action.equals("delete")) {
                deleteDeviceId();
            } else if (action.equals("update")) {
                String deviceId = args.getString(0);
                updateDeviceId(deviceId);
            } else if (action.equals("requestPermission")) {
                requestStoragePermissions();
            } else if (action.equals("checkPermission")) {
                checkStoragePermissions();
            } else {
                this.callbackContext.error("Invalid action");
                return false;
            }
        } catch (Exception e) {
            this.callbackContext.error("Exception occurred: ".concat(e.getMessage()));
            return false;
        }
        return true;
    }

    protected void saveDeviceId(String deviceId) {
        try {
            // define device id file on internal app directory
            Context context = cordova.getActivity().getApplicationContext();
            File deviceIdFile = getDeviceIdFile(context);
            if (!deviceIdFile.exists())
                writeDeviceIdFile(deviceIdFile, deviceId);

            this.callbackContext.success("Device Id saved successfully");
        } catch (Exception e) {
            this.callbackContext.error("Exception occurred: ".concat(e.getMessage()));
        }
    }

    protected void getDeviceId() {
        try {
            // define device id file on internal app directory
            Context context = cordova.getActivity().getApplicationContext();
            File deviceIdFile = getDeviceIdFile(context);
            String deviceId = "";
            if (deviceIdFile.exists())
                deviceId = readDeviceIdFile(deviceIdFile);

            this.callbackContext.success(deviceId);
        } catch (Exception e) {
            this.callbackContext.error("Exception occurred: ".concat(e.getMessage()));
        }
    }

    protected void updateDeviceId(String deviceId) {
        try {
            Context context = cordova.getActivity().getApplicationContext();
            File deviceIdFile = getDeviceIdFile(context);
            if (deviceIdFile.exists())
                writeDeviceIdFile(deviceIdFile, deviceId);

            this.callbackContext.success("Device Id file updated successfully");
        } catch (Exception e) {
            this.callbackContext.error("Exception occurred: ".concat(e.getMessage()));
        }
    }

    protected void deleteDeviceId() {
        try {
            // define device id file on internal app directory
            Context context = cordova.getActivity().getApplicationContext();
            File deviceIdFile = getDeviceIdFile(context);
            if (deviceIdFile.exists())
                deviceIdFile.delete();
            this.callbackContext.success("Device Id file deleted successfully");
        } catch (Exception e) {
            this.callbackContext.error("Exception occurred: ".concat(e.getMessage()));
        }
    }

    private static String readDeviceIdFile(File deviceIdFile) throws IOException {
        RandomAccessFile f = new RandomAccessFile(deviceIdFile, "r");
        byte[] bytes = new byte[(int) f.length()];
        f.readFully(bytes);
        f.close();
        return new String(bytes);
    }

    private static void writeDeviceIdFile(File deviceIdFile, String deviceId) throws IOException {
        FileOutputStream out = new FileOutputStream(deviceIdFile);
        out.write(deviceId.getBytes());
        out.close();
    }

    private static File getDeviceIdFile(Context context) {
        return new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), DEVICE_ID_FILE);
    }

    protected void requestStoragePermissions() {
        try {
            Activity activity = cordova.getActivity();
            // ActivityCompat.requestPermissions(
            //         activity,
            //         PERMISSIONS_STORAGE,
            //         REQUEST_EXTERNAL_STORAGE);
            Intent intent = new Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
            activity.startActivity(intent);

            this.callbackContext.success("Permissions requested with success");
        } catch (Exception e) {
            this.callbackContext.error("Exception occurred: ".concat(e.getMessage()));
        }
    }

    protected void checkStoragePermissions() {
        try {
            Context context = cordova.getActivity().getApplicationContext();
            File deviceIdFile = getDeviceIdFile(context);
            Boolean hasPermission = Environment.isExternalStorageManager(deviceIdFile);

            this.callbackContext.success(hasPermission.toString());
        } catch (Exception e) {
            this.callbackContext.error("Exception occurred: ".concat(e.getMessage()));
        }
    }
}
