# nyx-device-id

Cordova device id for Nyx devices

## Installation

`cordova plugin add https://github.com/john-doherty/cordova-unique-device-id.git`

## Supported Platforms

- Android
- iOS

## Usage

```js
// check the plugin is installed
if (window.plugins && window.plugins.uniqueDeviceID) {

    // request UUID
    plugins.uniqueDeviceID(function(uuid) {
        // got it!
        console.log(uuid);
    },
    function(err) {
        // something went wrong
        console.warn(err);
    });
}