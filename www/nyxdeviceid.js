var exec = require('cordova/exec');

module.exports = {
    get: function (success, fail) {
        cordova.exec(success, fail, 'NyxDeviceId', 'get', []);
    },
    save: function (deviceId, success, fail) {
        cordova.exec(success, fail, 'NyxDeviceId', 'save', [deviceId]);
    },
    delete: function (success, fail) {
        cordova.exec(success, fail, 'NyxDeviceId', 'delete', []);
    },
    update: function (deviceId, success, fail) {
        cordova.exec(success, fail, 'NyxDeviceId', 'update', [deviceId]);
    },
    requestPermission: function (success, fail) {
        cordova.exec(success, fail, 'NyxDeviceId', 'requestPermission', []);
    },
    checkPermission: function (success, fail) {
        cordova.exec(success, fail, 'NyxDeviceId', 'checkPermission', []);
    }
};